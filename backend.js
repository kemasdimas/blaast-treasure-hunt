var http = require('blaast/simple-http'),
    _ = require('underscore'),
    sys = require('sys'),
    jsdom = require('jsdom'),
    QS = require('querystring'),
    Scaling = require('blaast/scaling').Scaling,
    ReqLog  = require('blaast/mark').RequestLogger;
    
var storage = require('blaast/simple-data');

var scaling     = new Scaling(app.config);
var rlog        = new ReqLog(app.log);
var ShadowChess = { };

ShadowChess.players = { };
ShadowChess.rooms   = { };
ShadowChess.timeout = 1000 * 60 * 10; // Timeout in 10 minutes
ShadowChess.timeoutTolerance = 0; // Timeout tolerance is 1 minute

// Ranking object. Holds .scores, array for all scores.
function Ranking() {
	this.scores = [];
	this.inhale();
}

Ranking.prototype = {
	// Get score object by userid
	get: function(userId) {
		return _.detect(this.scores, function(s) { return s.userId === userId; });
	},

	// Set score of user
	set: function(userId, score) {
		var r = this.get(userId);
		if (r) {
			if (score > r.score) {
				r.score = score;
			}
		} else {
			this.scores.push({
				userId: userId,
				score: score
			});
		}
		this.scores = _.sortBy(this.scores, function(s) {
			return -s.score;
		});
	},

	// Add score of user
	add: function(userId, score) {
		var r = this.get(userId);
		if (r) {
            r.score += score;
            //we don't need score check as new score is appended to old score
			//if (score > r.score) {
			//	r.score += score;
			//}
		} else {
			this.scores.push({
				userId: userId,
				score: score
			});
		}
		this.scores = _.sortBy(this.scores, function(s) {
			return -s.score;
		});
	},

	// get score by rank
	getScore: function(rank) {
		if (rank >= 1 && rank-1 < this.scores.length) {
			return this.scores[rank-1].score;
		}
	},

	// get rank of user
	getRank: function(userId) {
		var r = this.get(userId);
		return r ? _.indexOf(this.scores, r) + 1 : 0;
	},

	toArray: function(limit) {
        var tmp = this.scores;
        tmp = _.sortBy( tmp, function(scoreItem){  return -parseInt(scoreItem.score, 10); });
		return tmp.slice(0, limit);
		//return this.scores;
	},

	// Read scores from persistent storage
	inhale: function() {
		var self = this;

		storage.get('score', function(err, value) {
			if (value && value.scores) {
				self.scores = value.scores;
				log.info('read scores, count=' + self.scores.length);
			} else {
				log.info('no scores found, value=' + JSON.stringify(value));
			}
		});
	},

	// Write scores to persistent storage
	exhale: function() {
		storage.set('score', { scores: this.scores }, function(err, oldData) {
			if (err) {
				log.info('Failed to store highscores: ' + err);
			} else {
				log.info('Stored highscores.');
			}
		});
	}
};

var globalRanking = new Ranking();
if (globalRanking.scores.length === 0) {
	globalRanking.set('CPU', 45);
}

function ShadowChessRoom(host, name, id) {
    var self = this;
    self.players = [ ];
    self.playersId = { };
    self.name = name;
    self.id = id;
    self.turn = -1;
    self.chatMessages = [];
    
    self.players.push(host);
    self.playersId[host.client.id] = 0;
}
ShadowChessRoom.prototype = {
    
    createRoom: function() {
        ShadowChess.rooms[this.id] = this;
    },
    
    joinRoom: function(guest) {
        if (this.playersId[guest.client.id] !== undefined) {
            return true;
        } else if (this.players.length < 2) {
            this.playersId[guest.client.id] = 1;
            this.players.push(guest);
            
            return true;
        } else {
            return false;
        }
    },
    
    leaveRoom: function(side) {
        var self = this;
        
        // Properly modify side if it is only one player in room
        if (side === 1 && self.players.length < 2) {
            side = 0;
        }
        var leavingPlayer = self.players[side];
        delete ShadowChess.players[leavingPlayer.client.id];
        
        // properly delete a player
        if (side === 1) {
            self.players.pop();   
        } else {
            self.players.shift();
        }
        delete self.playersId[leavingPlayer.client.id];
        
        // There will be only one player, send him a congratulation
		var cause = "";
        if (self.players.length > 0 && this.opponentLost) {
			log.info('Congratulate player ' + self.players[0].side);
			cause = 'Too late, the enemy plundered your treasures.';
			
			self.players[0].playerLose(cause);
			self.players[0].leaveRoom();
		} else if( self.players.length > 0 ) {
			log.info('Congratulate player ' + self.players[0].side);
			if (self.opponentTimeout) {
				cause = 'Opponent idle. ';
			} else if (self.opponentSurrender) {
				cause = 'Opponent surrender. ';
			} else {
				cause = 'Opponent tresures is plundered. ';
			}

			self.players[0].playerWin(cause);
			self.players[0].leaveRoom();
        } else {
            self.destroyRoom();
        }
    },
    
    destroyRoom: function() {
        clearInterval(this.timerInterval);
        delete ShadowChess.rooms[this.id];
        
        console.dir(ShadowChess);
    },
    
    playerReady: function(side, isResync) {
        if (this.players[side]) {
            this.players[side].isReady = true;
        }
        
        if (this.countReadyPlayer() >= 2) {
            
            // Broadcast the players data
            var lightName = this.players[0].client.user.id;
            var darkName = this.players[1].client.user.id;
            this.players.forEach(function(player) {
                player.setPlayers(lightName, darkName);
            });
            
            if (isResync) {
                this.players[this.turn].setTurn(this.getRemainingTimeout());
            } else {
            
                // If the room is newly created, must send prepareBoard first    
                this.turn = 0;
                this.players[0].prepareBoard();
                this.players[1].prepareBoard();
            }
        }
    },
    
    countReadyPlayer: function() {
        var count = 0;
        
        this.players.forEach(function(player) {
            if (player.isReady) {
                count++;
            }    
        });
        
        return count;
    },
    
    playerSurrender: function(side) {
        this.opponentSurrender = true;
        clearInterval(this.timerInterval);
                
        this.players[side].playerSurrender();
        this.players[side].leaveRoom();
    },
    
    playerWin: function(side) {
		this.opponentLost = true;
        clearInterval(this.timerInterval);
            
        this.players[side].playerWin("You have found the treasures first.");
        this.players[side].leaveRoom();
    },
    
    sendChatMessage: function(sender, text) {
        // Save the messages
        this.chatMessages.push({
            sender: sender,
            text: text
        });
        
        var destination = (sender === 0 ? 1 : 0);
        
        this.players.forEach(function(player) {
            player.sendChatMessage(sender, text); 
        });
    },
    
    syncMessage: function(requester) {
        this.players[requester].messageSynched(this.chatMessages);
    },
    
    resetTimeout: function() {
        var self = this;
        time = new Date().getTime();
        
        clearInterval(this.timerInterval);
        // this.timeoutBase = time;
        this.timeout = time + ShadowChess.timeout + ShadowChess.timeoutTolerance;
        
        // Interval executed each 10 seconds to provide tolerance
        this.timerInterval = setInterval(function() {
            if (self.isPlayerTimeout()) {
                self.opponentTimeout = true;
                clearInterval(self.timerInterval);
                
                self.players[self.turn].timeoutKick();
                self.players[self.turn].leaveRoom();
            }
            
            log.info('Timer is Ticking... -> ' + self.getRemainingTimeout());
        }, 10 * 1000);
    },
    
    isPlayerTimeout: function() {
        return (this.getRemainingTimeout() <= 0);
    },
    
    /**
     * Get remaining timeout in Second
     */
    getRemainingTimeout: function() {
        time = new Date().getTime();
        
        return Math.ceil((this.timeout - ShadowChess.timeoutTolerance - time) / 1000);
    },
    
    switchTurn: function(playerSide) {
        console.log('PlayerSide: ' + playerSide + ' TurnNow: ' + this.turn);
        if (playerSide === this.turn) {
            if (this.turn === 0) {
                this.turn = 1;
            } else {
                this.turn = 0;
            }
            
            this.resetTimeout();
            this.players[this.turn].setTurn(this.getRemainingTimeout());
        }
    },
    
    initBoard: function(map, side) {
        console.dir(map);
        this.players[side].map = map;
        
		opponentSide = (side === 0 ? 1 : 0);
		this.players[opponentSide].sendOpponentBoard(this.players[ side ].map, side);

        if (this.players[0].map !== undefined 
            && this.players[1].map !== undefined) {
            
            log.info('Both player ready!');
            this.initialized = true;
            
            this.resetTimeout();
            this.players[0].setTurn(this.getRemainingTimeout());
		}
    },
	
	moveCursor: function(index, side){
		opponentSide = (side === 0 ? 1 : 0);
		this.players[opponentSide].opponentMoveCursor(index, side);
	},
	
	digSoil: function(index, map, side){
		opponentSide = (side === 0 ? 1 : 0);
		this.players[opponentSide].opponentDigSoil(index, side);
		this.players[opponentSide].map = map;
		
	},
    
    resync: function(side) {
        this.players[side].resyncSuccess(this.map, this.turn);
    }
};

function ShadowChessPlayer(client) {
    var self = this;
    self.action = null;
    self.client = client;
    self.roomId = null;
    self.isHost = false;
    self.side = 1;
    self.score = 0;
}
ShadowChessPlayer.prototype = {
    /**
     * Create new room
     */
    createRoom: function() {
        var self = this;
        
        var timeStamp = new Date().getTime().toString();
        var room = new ShadowChessRoom(self, self.client.user.id, self.client.id + '-' + timeStamp);
        room.createRoom();
        
        self.isHost = true;
        self.side = 0;
        self.roomId = room.id;
        
        // Send created msg to client
        self.client.msg('startGame', { isHost: self.isHost, side: self.side, roomId: self.roomId });
    },
    
    /**
     * Used by the guest to join a room
     */
    joinRoom: function(args) {
        var self = this;
        var roomId = args.roomId;
        var resync = args.resync;
        if (resync) {
            roomId = ShadowChess.players[self.client.id].roomId;
        }
        
        var room = ShadowChess.rooms[roomId];
        if (room !== undefined) {
            if (room.joinRoom(self)) {
                self.roomId = roomId;
                
                if (resync) {
                    timeout = room.getRemainingTimeout();
					opponentSide = (self.side === 0 ? 1 : 0);
                    obj = { isHost: self.isHost, side: self.side, resync: { playerMap: room.players[self.side].map, opponentMap: room.players[opponentSide].map, turn: room.turn, timeout: timeout, roomId: self.roomId } };
                    console.dir(obj);
                    
                    self.client.msg('continueGame', obj);
                } else {
                    // Send joined msg to client
                    self.client.msg('startGame', { isHost: self.isHost, side: self.side, roomId: self.roomId });
                }
                
                return;
            }
        }
        
        // Failed to join room, delete player
        self.client.msg('noRoom', { });
        delete ShadowChess.players[self.client.id];
    },
    
    /**
     * Player leaving room
     */
    leaveRoom: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.leaveRoom(this.side);
        }
        
        log.info('Player ' + this.side + ' is leaving.');
    },
    
    setPlayers: function(light, dark) {
        var self = this;
        
        self.client.msg('setPlayers', { light: light, dark: dark });
    },
    
    /**
     * Used to initialize a board for each player, this message sent by the player
     * to the backend to indicate that he is ready to play (has burried the treasures)
     */
    initBoard: function(args) {
        var side = args.side;
        var map = args.map;
    
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.initBoard(map, side);
        }
    },
    
    resync: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.resync(this.side);
        }
    },
    
    resyncSuccess: function(map, turn) {
        // console.dir({ positions: positions, turn: turn });
        
        this.client.msg('resync', { map: map, turn: turn });
    },
    
    playerReady: function(args) {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.playerReady(args.side, args.resync);
        }
    },
    
    prepareBoard: function() {
        // TODO: this function should be called from room
        this.client.msg('prepareBoard', {  });
    },
    
    setTurn: function(remaining) {
        this.client.msg('setTurn', { side: this.side, remaining: remaining });
    },
    
    switchTurn: function(args) {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.switchTurn(args.side);
        }
    },
    
    handlePlayerSurrender: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.playerSurrender(this.side);
        }
    },
    
    handlePlayerWin: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
			opponentSide = (this.side === 0 ? 1 : 0);
			room.players[this.side].sendScore(room.players[opponentSide].map);
			room.players[opponentSide].sendScore(room.players[this.side].map);
			
            room.playerWin(this.side);
        }
    },
    
    handleChatMessage: function(args) {
        var side = args.side;
        var text = args.text;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.sendChatMessage(side, text);
        }
        
        this.client.msg('chatAcknowledge', { });
    },
    
    sendChatMessage: function(sender, text) {
        this.client.msg('chatMessage', { sender: sender, text: text });
    },
    
    messageSynched: function(messages) {
        console.dir(messages);
        this.client.msg('messageSynched', { messages: messages });
    },
    
    handleSyncMessage: function() {
        var self = this;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.syncMessage(self.side);
        }
    },
    
    playerSurrender: function() {
        this.opponentSurrender = true;
        this.client.msg('playerLose', { cause: ' You\'ve left the game.',  score: this.score });
    },
    
    playerWin: function(cause) {
        this.client.msg('playerWin', { score: this.score, cause: cause });
    },
    
    playerLose: function(cause) {
        this.client.msg('playerLose', { score: this.score, cause: cause });
    },
    
    timeoutKick: function() {
        this.client.msg('timeoutKick', { score: this.score, timeoutMax: Math.ceil(ShadowChess.timeout / 1000) });
    },
	
	sendOpponentBoard: function(map, side){
		this.client.msg('opponentBoard', { map: map, side: side});
	},
	
	moveCursor: function(args){
		var self = this;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.moveCursor(args.index, self.side);
        }
	},
	
	opponentMoveCursor: function(index, side){
		this.client.msg('moveCursor', { index: index, side: side});
	},
	
	digSoil: function(args){
		var self = this;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.digSoil(args.index, args.opponentMap, self.side);
        }
	},
	
	opponentDigSoil: function(index, side){
		this.client.msg('digSoil', { index: index, side: side});
	},
	
	sendScore: function(opponentMap){
		var self = this;
		score = 0;
		
		for (n = 0; n < 64; n++) {
            if(opponentMap[n] !== null && opponentMap[n].revealed){
				score++;
			}
        }

		globalRanking.add(self.client.user.id, score);
		rank = globalRanking.getRank(self.client.user.id);
		//var robj = globalRanking.get(self.client.user.id);
		globalRanking.dirty = true;
	},
	
	topScore: function(){
        var self = this;
		var latestRanking = globalRanking.toArray(20);
        console.dir( latestRanking );
        
        myscore = globalRanking.get( self.client.id );
        
        /***
		//this snippet is intended to cleanup domain part from userId (when email is the userId)
		for(var r in latestRanking){
			var _thisUser = latestRanking[r].userId.split("@");
			if(_thisUser[1]){
				latestRanking[r].userId = _thisUser[0] + "@" + "*****";
			}
			
		}
		console.dir( latestRanking );
		*/
        
        this.client.msg('topScore', {ranking: latestRanking, myscore: myscore});
	}
};

function fetchRooms(client) {
    var rooms = [];
    
    
    Object.keys(ShadowChess.rooms).forEach(function(key) {
        var room = ShadowChess.rooms[key];
        
        if (room.countReadyPlayer() < 2) {
            rooms.push({
                name: room.name,
                id: room.id
            });    
        }   
    });
    
    // log.info('ROOM FETCHED');
    client.msg('fetchRooms', { rooms: rooms });
}

app.message(function(client, action, args) {
    var self = this;
    app.debug(client.header() + ' action="' + action + '"');
    self.client = client;
    
    if (action === 'fetchRooms') {
        fetchRooms(client);
    } else if (action.length > 0 && action.charAt(0) !== '_' && ShadowChessPlayer.prototype.hasOwnProperty(action)) {
        if (ShadowChess.players[client.id] === undefined) {
            ShadowChess.players[client.id] = new ShadowChessPlayer(client);
        }
        
        var user = ShadowChess.players[client.id];
        user.action = action;
        user[action].call(user, args);
    } else {
        app.debug(client.header() + ' unknown-action="' + action + '"');
    }
    
    // console.dir(ShadowChess);
});


// Every thirty seconds, write scores to db if changes happened.
setInterval(function() {
	if (globalRanking.dirty) {
		delete globalRanking.dirty;
		globalRanking.exhale();
	}
}, 5 * 1000);
