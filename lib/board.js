var _ = require('common/util');
var app = this;

var kClear = -1;
var row = function(index) {
    return Math.floor(index / 8);  
};
var col = function(index) {
    return index % 8;
};

var MAX_COINS = 5;
var MAX_CHESTS = 2;
var CHEST_BLOCK = [0, 1, 8, 9];

/**
 * Board should be instantiated 2 times, for current player
 * and also for opponent, with exact same logic.
 */
function Board() {
	this.map = [];
	this.trash = [];
	this.modified = [];
	this.side = 0;
	
	// Tresaure indicators
	this.coinsPlaced = 0;
	this.chestsPlaced = 0;
	
	this.coinsFound = 0;
	this.chestsFound = 0;
	
	this.nodeType = {
	    coin: 0,
	    chest: 1
    };
	
	this.playerType = {
	    white: 0,
	    black: 1
	};
}

Board.prototype = {
	at: function(x, y) {
		if (y === undefined) {
			return this.map[x];
		}
		return this.map[y * 8 + x];
	},
	
	// (y * 8) + x
	xy: function(index) {
		return {
			x: Math.floor(index % 8),
			y: Math.floor(index / 8)
		};
	},

    resync: function(remoteMap, storedSide) {
        console.log('Resync the opponents map!');
        this.map = [];
        this.side = storedSide;
        
        var map = this.map;
        for (n = 0; n < 64; n++) {
            map.push(remoteMap[n]);
        }
    },

    init: function(storedSide) {
        console.log('Board for player ' + storedSide + ' inited.');
        this.side = storedSide;
        
        this.map = [];
        for (n = 0; n < 64; n++) {
            this.map.push(null);
        }
    },
    
    isSquareAvailable: function(index, type) {
        var nodeType = this.nodeType;
        
        if (type === nodeType.coin) {
            return (this.map[index] === null);    
        } else if (type === nodeType.chest) {
            for (n = 0; n < 4; n++) { 
                tempIndex = index + CHEST_BLOCK[n];
                
                if (this.map[tempIndex] !== null) {
                    return false;
                }
            }
        }
        
        return true;
    },
    
    digTreasure: function(index) {
        var node = this.map[index];
        console.log('DIG INSIDE');
        
        if (node !== null) {
            if (node.type === this.nodeType.coin) {
                console.log('revealed inside');
                node.revealed = true;
                this.coinsFound++;
            } else if (node.type === this.nodeType.chest) {
                console.log('peeked inside');
                node.peeked = true;
                
                allRevealed = true;
                tempIndex = index - CHEST_BLOCK[node.spriteType - 1];
                for (n = 0; n < 4; n++) {
                    allRevealed = allRevealed && this.map[tempIndex + CHEST_BLOCK[n]].peeked;
                }
                
                if (allRevealed) {
                    this.chestsFound++;
                    for (n = 0; n < 4; n++) {
                        this.map[tempIndex + CHEST_BLOCK[n]].revealed = true;
                    }
                }
            }
        }
    },
    
    buryTreasure: function(index, type) {
        var nodeType = this.nodeType;
        var placed = true;
        var node = this.map[index];
        if (this.isSquareAvailable(index, type)) {
            if (type === nodeType.coin) {
                this.map[index] = {
                    id: index,
                    type: type,
                    spriteType: 0,
                    revealed: false,
                    peeked: false
                };
            } else if (type === nodeType.chest &&
                col(index) < 7 && row(index) < 7) {
                
                for (n = 0; n < 4; n++) {
                    this.map[index + CHEST_BLOCK[n]] = {
                        id: index + CHEST_BLOCK[n],
                        type: type,
                        spriteType: type + n,
                        revealed: false,
                        peeked: false
                    };
                }
            } else {
                console.log('Index out of bound');
                placed = false;
            }
        } else {
            console.log('Index occuppied');
            placed = false;
        }
        
        if (placed) {
            // TODO: must be placed properly, then iterate the counter
            if (type === nodeType.coin) {
                console.log('Coin burried');
                this.coinsPlaced++;
            } else if (type === nodeType.chest) {
                console.log('Chest burried');
                this.chestsPlaced++;
            } else {
                console.log('TREASURE TYPE NOT SUPPORTED');
            }
        }
    },
    
    getRemainingCoins: function() {
        return MAX_COINS - this.coinsPlaced;
    },
    
    getRemainingChests: function() {
        return MAX_CHESTS - this.chestsPlaced;
    },
    
    isBoardReady: function() {
        return (this.getRemainingCoins() === 0 && this.getRemainingChests() === 0);
    },
    
    isTreasureEmpty: function() {
        return (this.coinsFound === MAX_COINS) && (this.chestsFound === MAX_CHESTS);
    }
};

exports.Board = Board;
