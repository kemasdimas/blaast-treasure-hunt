var app = this;

var K_X = 15;
var K_Y = 19;

exports.setup = function(scene) {
	scene.defineSpritesheet('numberfont', app.resourceURL('setnumber.png'), K_X, K_Y);
};

function TextObject(scene, layer, x, y) {
	this.scene = scene;
	this.layer = layer;
	this.basex = x;
	this.basey = y;
	this.objs  = [];
}

exports.TextObject = TextObject;

TextObject.prototype = {
	set: function(value) {
		var i;
		var scene = this.scene;

		if (value.length !== this.objs.length) {
			while (value.length < this.objs.length) {
				scene.remove(this.objs.pop());
			}

			for (i = this.objs.length; i < value.length; i++) {
				this.objs.push(scene.add({
					sprite: 'numberfont',
					layer: this.layer
				}));
			}

			this.moveTo({ x: this.basex, y: this.basey });
		}

        var temp;
        var index;
		for (i = 0; i < value.length; i++) {
		    temp = value.charAt(i);
		    
		    if (temp === ':') {
		        index = 10;
		    } else {
		        index = parseInt(temp, 10);
		    }
		    
			scene.change(this.objs[i], {
				frame: index
			});
		}
	},

	moveTo: function(opts) {
		var basex  = opts.x;
		var basey  = opts.y;
		this.basex = basex;
		this.basey = basey;

		var k = (this.objs.length * K_X) / 2;

		for (var i = 0; i < this.objs.length; i++) {
			this.scene.change(this.objs[i], {
				x: basex - k + i * K_X,
				y: basey
			});
		}
	}
};
