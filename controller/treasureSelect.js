var ui = require('ui'),
    _ = require('common/util'),
    Board = require('./lib/board').Board,
    Particles = require('./lib/particles').Particles,
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
    
_.extend(exports, {
    init: function() {
        var self = this;
        var nodeType = self.board.nodeType;
        var types = [];
        
        if (self.board.getRemainingCoins() > 0) {
            types.push({
                name: 'Gold Coin (x' +  self.board.getRemainingCoins() + ')',
                type: nodeType.coin
            });
        }
        if (self.board.getRemainingChests() > 0) {
            types.push( {
                name: 'Treasure Chest (x' + self.board.getRemainingChests() + ')',
                type: nodeType.chest
            });
        }
        
        var list = [ ];
        var reuse;
        var counter = 0;
        types.forEach(function(item) {
            var stripeColor = (counter++ % 2 !== 0 ? '#ab7c52' : '#8e6744');
            
            reuse = new TextView({
                label: item.name,
                style: {
                    width: 'fill-parent',
                    border: '4 10'
                }
            });
            reuse.type = item.type;
            
            reuse.on('focus', function() {
                this.style(app.mystyle.listFocus());
            });
            reuse.on('blur', function() {
                this.style(app.mystyle.listBlur(stripeColor));
            });
            
            reuse.callback = function() {
                self.placeTreasure(item.type, self.boardIndex);
            };
            
            list.push(reuse);
        });
        
        var listView = new ListView(list, 'What to hide?');
        self.add('list', listView);
    },
    
    ':load': function() {
        var self = this;
        
        app.on('message', function(action, data) {
        });
        
        /*
        self.on('back', function() {
           console.log('Do nothing, back only when treasure type is selected'); 
        });*/
    },
    
    ':state': function(data) {
        this.parent = data.parent;
        this.boardIndex = data.index;
        this.board = data.board;
        
        this.isSelected = false;
        this.init();
    },
    
    ':keypress': function(key) {
        
        // Delegate up, down, fire, b, B, t, T to ListView
        // b, B used to jump to the bottom of the list
        // t, T used to jump to the top of the list
        if (!this.isSelected && (key === 'up' || key === 'down'
               || key === '84' || key === '116'
               || key === '66' || key === '98' 
               || key === 'fire')) {
                   
           this.get('list').emit('keypress', key);   
        }
    },
    
    ':inactive': function() {
        this.clear();  
        delete this.parent;
    },
    
    placeTreasure: function(type, index) {
        var self = this;
        console.log('place ' + type + ' on ' + index);
        
        self.board.buryTreasure(index, type);
        app.popView();
    },
    
    backToGame: function() {
        this.parent.updateGems();
        this.parent.switchTurn();    
        
        app.popView();
    }
});