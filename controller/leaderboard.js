var ui = require('ui'),
    _ = require('common/util'),
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    SceneView = ui.SceneView,
    ListView = require('../lib/ListView').ListView;
 
var app = this;
    
var widthL = 320;
var transY=10;
    
_.extend(exports, {
    ':load': function() {
        var self = this;
        var scene = new SceneView({
            style: {
                width: 'fill-parent', 
                height: 'fill-parent'
            }
        });
        self.add('scene', scene);
        
        // Create single layer SceneView
        scene.setLayers(2);
      //  scene.defineSpritesheet('image', app.resourceURL('help.png'), imageSize.width, imageSize.height);
         app.on('message', function(action, data) {          
            if (action === 'topScore') {
				self.buildScore(data.ranking);
            } 
        });
		
    },

    ':active': function() {  
       
	
    },
    ':resized': function(width, height) {
        var self = this;
        var scene = self.get('scene');  
		if(width>250){
		scene.defineSpritesheet('bg_leader', app.resourceURL('bg_leader.png'), 320, 240);  
        }else{
		scene.defineSpritesheet('bg_leader', app.resourceURL('bg_leader_portrait.png'), 240, 320);  
		}
		scene.setLayerBackground(0, {
            sprite: 'bg_leader',
            x: 0,
            y: 0,
            width: width,
            height: height
        });
		
		
        
        if (width < height) {
          
            self.transY = 40;
        }else{
		 self.transY = 10;
		}
		
		self.widthL=width;
			
		app.msg('topScore', { });
    },
	
    ':keypress': function(key) {
    }, 
	buildScore: function(scores) {
	
		var self = this;
		
        var list = [];
        var reuse;
        var counter = 1;
		var scene = self.get('scene');

        // Iterate rooms
        scores.forEach(function(score) {
           
            reuse = new TextView({
                label: 'No '+ (counter++) +'. '+score.userId+' = '+score.score,
                style: {
                    width: 'fill-parent',
                    border: '4 10'
                }
            });
			reuse.style({
			'font-weight': 'bold'
			});
            reuse.on('focus', function() {
               
            });
            reuse.on('blur', function() {
                
            });
            list.push(reuse);
        });
        
        var listView = new ListView(list,null);
		
		 // Add the menu to scne

        scene.setLayerControl(1,listView);
		console.log(self.widthL);
		console.log(self.transY);
		scene.translate(1, (self.widthL - 150) / 2, self.transY + 50);
		       
    }
});

