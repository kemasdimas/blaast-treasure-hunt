var ui = require('ui'),
    _ = require('common/util'),
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    SceneView = ui.SceneView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
app.chess = { };
app.chess.gameTypes = {
    training: 0,
    multiplayer: 1  
};
app.chess.storage = app.storage('chess');

introLayers = {
    background: 0,
    logo: 1,
    menu: 2,
    signature: 3
}; 

_.extend(exports, {

    ':load': function() {
        var self = this;
        
        self.add('scene', new SceneView({
            style: {
                width: 'fill-parent',
                height: 'fill-parent'
            }
        }));
        var scene = self.get('scene');
        scene.setLayers(4);
        
       // scene.defineSpritesheet('bgtile', app.resourceURL('bgtile2.png'), 53, 54);
        scene.defineSpritesheet('bglands', app.resourceURL('menu_bg.png'), 320, 240);
         scene.defineSpritesheet('bgpot', app.resourceURL('menu_bg_pot.png'), 240, 320);
      //  scene.defineSpritesheet('logo', app.resourceURL('biglogo.png'), 220, 92);
        //scene.defineSpritesheet('signature', app.resourceURL('signature.png'), 95, 20);
        
        // Set background
      //  scene.setLayerBackground(introLayers.background, {
      //      sprite: 'bglands',
      //      x: 0,
      //      y: 0
      //  });
        
        // Set logo
        /*self.logo = scene.add({
            sprite: 'logo',
            x: 0,
            y: 0,
            layer: introLayers.logo,
            frame: 0
        });
        
        // Set signature
        self.logo = scene.add({
            sprite: 'signature',
            x: 0,
            y: 0,
            layer: introLayers.signature,
            frame: 0
        });
        */
        // self.get('image').src(app.resourceURL('biglogo.png'));
        
        var menus = [{
            text: 'Play Multiplayer',
            image: 'menu_play.png',
            callback: function() {
                app.isBackFromGame = false;
                self.startMultiplayer();
            }
        }, {
            text: 'How to Play',
            image: 'menu_how.png',
            callback: function() {
                app.pushView('howto', { });
            }
        }, {
            text: 'Leader Board',
            image: 'menu_leader.png',
            callback: function() {
                app.pushView('leaderboard', { });
            }
        }];
        
        var list = [ ];
        var reuse;
        menus.forEach(function(item) {
            reuse = new SceneView({
                style: {
                    width: 154,
                    height: 30
                }
            });
            reuse.setLayers(1);
            reuse.defineSpritesheet('background', app.resourceURL(item.image), 149, 19);
            
            var bg = reuse.add({
                sprite: 'background',
                x: 0,
                y: 0,
                layer: 0,
                frame: 1
            });
            
            reuse.on('focus', function() {
                reuse.change(bg, {
                    frame: 1
                });
            });
            reuse.on('blur', function() {
                reuse.change(bg, {
                    frame: 0
                });
            });
            
            reuse.callback = item.callback;
            
            list.push(reuse);
        });
        
        self.listView = new ListView(list, null);
        
        // Add the menu to scne
        scene.setLayerControl(introLayers.menu, self.listView);
        
        app.on('message', function(action, data) {
            if (action === 'noRoom') {
                try {
                    app.chess.storage.set('roomId', { });
                    self.startMultiplayer();
                } catch (error) { }
            } else if (action === 'continueGame') {                
                app.pushView('main', { 
                    gameType: app.chess.gameTypes.multiplayer, 
                    playerSide: data.side,
                    resync: true,
                    playerMap: data.resync.playerMap,
                    opponentMap: data.resync.opponentMap,
                    turn: data.resync.turn });
            }
        });
    },
    
    ':resized': function(width, height) {
        var self = this;
        var scene = self.get('scene');
        var isPortrait = false;
        var transY = 10;
        
        if (width < height) {
            isProtrait = true;
            transY = 40;
        }
        console.log(isPortrait);
        if(width===240){

		scene.setLayerBackground(introLayers.background, {
            sprite: 'bgpot',
            x: 0,
            y: 0
        });
		}else{
		
		scene.setLayerBackground(introLayers.background, {
            sprite: 'bglands',
            x: 0,
            y: 0
        });	
		}
		
        scene.changeLayer(introLayers.background, {
            width: width, 
            height: height
        });
		
		
       // scene.translate(introLayers.logo, (width - 220) / 2, transY);
        scene.translate(introLayers.menu, (width - 153) / 2, transY + 100);
        //scene.translate(introLayers.signature, (width - 90) / 2, height - 25);
    },
    
    startMultiplayer: function() {
        console.log('ROOM ID: ' + app.chess.storage.get('roomId'));
        var roomId = app.chess.storage.get('roomId');
        if (roomId.roomId) {
            app.msg('joinRoom', { resync: true });
        } else {
            app.pushView('roomSelect', { });
        }
    },
    
    ':keypress': function(key) {
        
        // Delegate up, down, fire, b, B, t, T to ListView
        // b, B used to jump to the bottom of the list
        // t, T used to jump to the top of the list
        if (!this.isSelected && (key === 'up' || key === 'down'
               || key === '84' || key === '116'
               || key === '66' || key === '98' 
               || key === 'fire')) {
                   
           this.listView.emit('keypress', key);   
        }
    }
});
