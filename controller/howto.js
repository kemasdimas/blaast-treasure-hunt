var ui = require('ui'),
    _ = require('common/util'),
    ImageView = ui.ImageView,
    SceneView = ui.SceneView;
    
var app = this;
    
var imageSize = { width: 240, height: 400 };
var K = 30;
    
_.extend(exports, {
    ':load': function() {
        var self = this;
        var scene = new SceneView({
            style: {
                width: 'fill-parent', 
                height: 'fill-parent'
            }
        });
        self.add('scene', scene);
        
        // Create single layer SceneView
        scene.setLayers(2);
        scene.defineSpritesheet('image', app.resourceURL('help.png'), imageSize.width, imageSize.height);
        scene.defineSpritesheet('bgtile', app.resourceURL('bg_how.png'), 32, 32);
        
        self.x = 0;
        self.y = 0;
    },

    ':active': function() {  
        try {
            this.y = 0;
            this.get('scene').translate(1, this.x, 0);
        } catch (err) { }
    },

    ':resized': function(width, height) {
        this.subLayout(width, height);
    },

    subLayout: function(width, height) {
        console.log('SUBLAYOUTING');
        var self = this;
        var scene = self.get('scene');
        
        if (width > height) {
            // Is landscape
            self.x = (width - imageSize.width) / 2;
        } else {
            self.x = 0;
        }
        
        scene.setLayerBackground(0, {
            sprite: 'bgtile',
            x: 0,
            y: 0,
            width: width,
            height: height,
            tile: true
        });
        
        scene.setLayerBackground(1, {
            sprite: 'image',
            x: self.x,
            y: self.y,
            width: imageSize.width,
            height: imageSize.height
        });
    },

    ':keypress': function(key) {
        if (key === 'up') {
            this.moveY(-K);
        } else if (key === 'down') {
            this.moveY(K);
        } else if (key === 'fire') {
            app.popView();
        } else {
            return;
        }
    },

    moveY: function(d) {
        var maxScrollable = imageSize.height - this.dimensions().height;

        // adjust our Y position and then animate to new position
        this.y += d;

        if (this.y < 0) {
            this.y = 0;
        } else if (this.y >= maxScrollable) {
            this.y = maxScrollable;
        }

        this.move();
    },

    move: function() {
        // We ask the SceneView to move the background layer to our
        // current position. By default it will use the scrolling easing
        // with a 400ms animation duration.

        this.get('scene').animate(1, {
            y: -this.y
        });
    }
});
