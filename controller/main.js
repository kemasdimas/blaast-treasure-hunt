var ui = require('ui'),
    _ = require('common/util'),
    Board = require('./lib/board').Board,
    
    Particles = require('./lib/particles').Particles,
    ImageView = ui.ImageView,
    SceneView = ui.SceneView,
    TextView = ui.TextView;

var app = this;
// TODO: board must be instantiated 2 times
// app.chess.board = new Board();

app.chess.board = {};
app.chess.board.player = new Board();
app.chess.board.opponent = new Board();

// app.chess.turn = app.chess.board.playerType.white;
app.chess.isTurn = function() {
    return app.chess.turn === app.chess.playerSide;
};
app.chess.inMinute = function(second) {
    minute = Math.floor(second / 60);
    second = second % 60;
    
    if (second < 10) {
        second = '0' + second.toString();
    }
    
    return minute + ':' + second;
};

var gameStates = {
	reset: -1,
	joining: 0,
    preparing: 1,
    waiting: 2,
    playing: 3,
    win: 4,
    lose: 5,
    chatting: 6
};
app.chess.state = gameStates.reset;

var layer = {
    checkboard:     0,
    gems:           1,
    pointer:        2,
    movemarker:     3,
    bgtile:         4,
    hud1:           5,
    hud2:           6,
    hud3:           7,
    energy1:        8,
    energy2:        9,
    profile:        10,
    particles:      11,
    notification:   12,
    notifcontent:   13,
    chatnotif:      14
};

var boardOffsetX = 80;
var gemWidth = 30;
var gemHeight = 30;
var notifHeight = 90;
var font = require('../lib/font');

function rand(n) {
    return Math.floor(Math.random() * n);
}

_.extend(exports, {
    scene: function() {
        return this.get('scene');
    },
    
    ':load': function() {
        var self = this;
        
        app.on('message', function(action, data) {
            try {
                // TODO: handle win / lose
                if (action === 'setPlayers') {
                    self.setPlayers(data.light, data.dark);
					app.chess.resetChat = true;
				} else if (action === 'opponentBoard') {
					console.log('Opponent board received ' + data.map + ' side ' + data.side);
					app.chess.board.opponent.resync(data.map, data.side);
				} else if (action === 'moveCursor') {
					// Show opponent movement!
					app.chess.state = gameStates.playing;
					self.hideNotification();
					self.markMove(data.index);
				} else if (action === 'digSoil') {
					console.log('REMOTE DIG!');
					app.chess.state = gameStates.playing;
					self.hideNotification();
					self.digSoil(app.chess.board.player, data.index, true);
				} else if (action === 'setTurn') {
					self.hideNotification();
					app.chess.state = gameStates.playing;
					self.setTurn(data.side);
					self.setTimeout(data.remaining);
                } else if (action === 'prepareBoard') {
					app.chess.state = gameStates.preparing;
					self.showPointer();
					self.showNotification("Burry your treasure! Don't let the others found it...");
                } else if (action === 'playerWin') {
                    app.chess.state = gameStates.win;
                    self.activateProfile();
                    
                    self.showNotification(data.cause + 'Congratulation, You WIN.');
                } else if (action === 'timeoutKick') {
                    app.chess.state = gameStates.lose;
                    self.deactivateProfile();
                    
                    self.showNotification('Game over. You\'ve been idle for ' + data.timeoutMax + 's');
                } else if (action === 'playerLose') {
                    app.chess.state = gameStates.lose;
                    self.deactivateProfile();
                    
                    self.showNotification('Game over.' + data.cause);
                } else if (action === 'chatMessage') {
                    if (data.sender !== app.chess.playerSide && app.chess.state !== gameStates.chatting) {
                        self.showChatNotif();
                    }
				} else {
					console.log('What is ' + action + '?');
				}
            } catch (err) {
                console.log('Caught Exception: ' + err);
            }
        });
        
        self.on('back', function() {            
            app.isBackFromGame = true;
	
			self.clear();
			self.clearTimeout();
			self.exitGame();
        });
    },
    
    ':state': function(state) {
        app.chess.gameType = state.gameType;
        app.isError = false;
        app.chess.state = -1;
        
        // Set the player side
        if (this.isMultiplayer() && app.isConnected) {
            app.chess.playerSide = state.playerSide;
            app.chess.turn = -1;
            
            app.chess.board.player = new Board();
			app.chess.board.opponent = new Board();
            
            console.log('INIT PLAYER SIDE: ' + app.chess.playerSide);
            if (state.resync) {
				console.log('CONTINUE BRO!');
			
				// When player resume a paused game, the game must be resynched with backend
				app.chess.resync = true;
				app.chess.board.player.resync(state.playerMap, 0);
				app.chess.board.opponent.resync(state.opponentMap, 1);
            } else {
				// Player decided to create a new room / join a created room
				app.chess.resync = false;
				app.chess.board.player.init(app.chess.playerSide);
				
				// TODO: initBoard should be sent after the treasure is placed
				// app.msg('initBoard', { map: app.chess.board.map });
            }
            
        } else if (this.isMultiplayer() && !app.isConnected) { 
            console.log('Connection not ready!');
            
            this.add(new TextView({
                label: 'Connection error, please press BACK and try again',
                style: {
                    width: 'fill-parent',
                    height: 'fill-parent',
                    align: 'center',
                    border: '30 0',
                    'font-weight': 'bold'
                }
            }));
            
            app.isError = true;
            return;
        } else {
            
            // Offline training
            // DISABLED
            // app.chess.board.init();
            console.log('');
        }
        
        this.init();
    },
	
	':active': function() {
		var self = this;
	
		if (app.chess.state === gameStates.chatting) {
			// Application returned from chatView
			app.chess.state = app.chess.prevState;
			console.log('BACK FROM CHAT');
		} else if (app.chess.state === gameStates.preparing) {
			this.updateGems(app.chess.board.player);
			
			// Back from treasure select, check whether every treasure have been burried
			if (app.chess.board.player.isBoardReady()) {
				app.chess.state = gameStates.waiting;
				self.hidePointer();
				self.showNotification('Please wait for other player to bury their treasure...');
				
				app.msg('initBoard', { map: app.chess.board.player.map, side: app.chess.playerSide });
			}
		}
	},
	
	init: function() {
	    var view = this;
        
        // Add the SceneView
        this.add('scene', new SceneView({
            style: {
                width: 'fill-parent',
                height: 'fill-parent'
            }
        }));
        
        this.selected = 0;
        var scene = this.scene();
        this.particles = new Particles(scene);
        
        scene.setLayers(16);
        
        // Background
        scene.defineSpritesheet('boardtile', app.resourceURL('play_square_sprite.png'), gemWidth, gemHeight);
        
        // Define SpriteSheet
        scene.defineSpritesheet('light', app.resourceURL('treasure_object.png'), gemWidth, gemHeight);
        scene.defineSpritesheet('revealed', app.resourceURL('digged_board_tile.png'), gemWidth, gemHeight);
        
        scene.defineSpritesheet('particles', app.resourceURL('particles.png'), 8, 8);
        scene.defineSpritesheet('pointer', app.resourceURL('play_shovel.png'), 42, 42);
        scene.defineSpritesheet('movemarker', app.resourceURL('pointer.png'), 42, 42);
        scene.defineSpritesheet('bgtile', app.resourceURL('bgtile.png'), 60, 60);
        scene.defineSpritesheet('rank', app.resourceURL('rank.png'), 47, 20);
        scene.defineSpritesheet('time', app.resourceURL('time.png'), 47, 20);
        scene.defineSpritesheet('score', app.resourceURL('score.png'), 47, 20);
        scene.defineSpritesheet('chatnotif', app.resourceURL('chatnotif.png'), 53, 24);
        scene.defineSpritesheet('notifpanel', app.resourceURL('notif.png'), 240, 90);
        
        // avatars
        scene.defineSpritesheet('lightAva', app.resourceURL('play_ava_A.png'), 53, 53);
        scene.defineSpritesheet('darkAva', app.resourceURL('play_ava_B.png'), 53, 53);
        
        for (n = 0; n < 64; n++) {
            y = Math.floor(n / 8);
            scene.add({
				sprite: 'boardtile',
				x: gemWidth * Math.floor((n + (y % 2)) % 8),
				y: gemHeight * y,
				layer: layer.checkboard,
				width: gemWidth,
				height: gemHeight,
				frame: 1
            });
        }
        
        this.selObj = scene.add({
            sprite: 'pointer',
            x: -6,
            y: -6,
            layer: layer.pointer,
            frame: 0
        });
        this.moveMarker = scene.add({
            sprite: 'movemarker',
            x: -6,
            y: -6,
            layer: layer.movemarker,
            frame: 0
        });
        scene.changeLayer(layer.movemarker, {
            visible: false 
        });
        if (!app.chess.isTurn()) {
            scene.changeLayer(layer.pointer, {
                visible: false 
            });
        }
        
        // Avatar placeholder
        var avaSprite = (app.chess.playerSide === 0 ? 'lightAva' : 'darkAva');
        this.profileHolder = scene.add({
            sprite: avaSprite,
            x: 0,
            y: 0,
            layer: layer.profile,
            width: 53,
            height: 53,
            frame: 1
        });
        this.chatNotification = scene.add({
            sprite: 'chatnotif',
            x: 0,
            y: 35,
            layer: layer.chatnotif,
            width: 53,
            height: 25,
            frame: 0
        });
        scene.changeLayer(layer.chatnotif, {
            visible: false 
        });
        
        this.scoreLabel = scene.add({
            sprite: 'score',
            x: 16,
            y: 90,
            layer: layer.hud1,
            frame: 0
        });
        this.rankLabel = scene.add({
            sprite: 'rank',
            x: 21,
            y: 140,
            layer: layer.hud1,
            frame: 0
        });
        this.timeLabel = scene.add({
            sprite: 'time',
            x: 21,
            y: 200,
            layer: layer.hud2,
            frame: 0
        });
        
        scene.setLayerBackground(layer.bgtile, {
            sprite: 'bgtile',
            x: 0,
            y: 0,
            tile: true
        });

        scene.add({
            sprite: 'notifpanel',
            x: 0,
            y: 0,
            layer: layer.notification,
            width: 240,
            height: notifHeight,
            frame: 0
        });
        
        scene.notificationContent = new TextView({
            label: 'Bismillah',
            style: {
                border: '15 15',
                width: 230,
                align: 'center',
                'font-size': 'small'
            }
        });
        scene.setLayerControl(layer.notifcontent, scene.notificationContent);
        
        // Font setup
        font.setup(scene);
        this.currentScore = new (font.TextObject)(scene, layer.hud1, 15, 70);
        this.currentScore.set('99999');
        this.currentRank = new (font.TextObject)(scene, layer.hud1, 15, 120);
        this.currentRank.set('99');
        this.currentTime = new (font.TextObject)(scene, layer.hud2, 15, 200);
        this.currentTime.set('10:00');
        
        // Hide the HUD1, only show the timer
        scene.changeLayer(layer.hud1, {
            visible: false
        });
        
        if (this.dimensions() !== undefined) {
            dimensions = this.dimensions();
            
            if (dimensions.width !== -1 && dimensions.height !== -1) {
                this.subLayout(dimensions.width, dimensions.height);
            }
        }
        
        // Inform ready to server, must be done after all rendering completed
        app.msg('playerReady', { side: app.chess.playerSide, resync: app.chess.resync });
	},
	
	animateLayers: function(open) {
        var scene = this.scene();
        var d     = this.dimensions();
        var dims  = this.dims;

        var neededWidth = gemWidth * 8;
        var x = width - neededWidth;

        var f = open ? function(v) { return v; } : this.closed_f;

        scene.animate(layer.profile, f(dims.profile));
        scene.animate(layer.chatnotif, f(dims.profile));
        scene.animate(layer.energy1, f(dims.energy));
        scene.animate(layer.energy2, f(dims.energy));
        scene.animate(layer.hud1, f(dims.hud1));
        scene.animate(layer.hud2, f(dims.hud2));
    
        if (d.width > d.height) {
            // Is landscape
            scene.animate(layer.bgtile, f({ x: -(8 * gemWidth + 1) }));
        } else {
            // Is portrait
            scene.animate(layer.bgtile, f({ y: (8 * gemWidth + 1) }));
        }
        this.showNotification();
    },
	
	':resized': function(width, height) {	    
	    this.subLayout(width, height);
	},

	':keypress': function(key) {
		var self		= this;
		var isTurn		= app.chess.isTurn();
		var isPreparing	= self.isPreparingBoard();
	    
	    if (self.isNotificationShown && app.chess.state !== gameStates.waiting) {
	        self.hideNotification();
	        return;
	    }
	    
	    // TODO: handle 1 (chat) / 9 (exit game / surrender)
	    if (key === '49') {
			app.chess.prevState = app.chess.state;
			app.chess.state = gameStates.chatting;
			self.showChatScreen();
			self.hideChatNotif();
	    } else if (key === '57') {
	        list = [{
	            name: 'Yes',
	            callback: function() {
	                app.msg('handlePlayerSurrender', { });
	            }
	        }, {
	            name: 'No'
	        }];
	        app.pushView('confirmSelect', { parent: self, list: list, title: 'Are you sure to quit game?' });
	    }
	    
	    var isMove = false;
	    var isDig = false;
		if (key === 'up' && this.selected >= 8 && (isTurn || isPreparing)) {
			isMove = true;
			this.select(this.selected - 8);
		} else if (key === 'down' && this.selected < 56 && (isTurn || isPreparing)) {
			isMove = true;
			this.select(this.selected + 8);
	    } else if (key === 'left' && this.selected > 0 && (isTurn || isPreparing)) {
	        isMove = true;
	        this.select(this.selected - 
	            Math.floor(this.selected % 8) + Math.floor((this.selected - 1) % 8));
	    } else if (key === 'right' && (isTurn || isPreparing)) {
	        isMove = true;
	        this.select(this.selected - 
	            Math.floor(this.selected % 8) + Math.floor((this.selected + 1) % 8));
	    } else if (key === 'fire' && (isTurn || isPreparing)) {
			// Fire button pressed & isTurn, must be digging the soil to find a treasure
			console.log('Tile chosen');
			
			if (isPreparing) {
				console.log('Show treasure select screen');
				app.pushView('treasureSelect', { parent: self, index: this.selected, board: app.chess.board.player });
			} else if (app.chess.state === gameStates.playing) {
				isDig = true;
			}
		}
		
		if (isMove && isTurn) {
			app.msg('moveCursor', { index: this.selected });
		} else if (isDig && isTurn) {
			self.digSoil(app.chess.board.opponent, this.selected, false);
			// switch turn
		}
	},
	
	exitGame: function() {
	   var self = this;
	   
	   app.pushView({
	       view: 'intro',
	       replace: 'current'
	   });  
	},
	
	isMultiplayer: function() {
	    return app.chess.gameType === app.chess.gameTypes.multiplayer;
	},
	
	isWhiteSide: function() {
	    return app.chess.playerSide === app.chess.board.player.playerType.white;
	},
	
	isBlackSide: function() {
        return app.chess.playerSide === app.chess.board.player.playerType.black;
    },
	
	isPreparingBoard: function() {
		return app.chess.state === gameStates.preparing;
	},
	
	subLayout: function(width, height) {
	    if (app.isError) {
	        return;
	    }
	    
        var scene = this.scene();

        var neededWidth = gemWidth * 8;
        var x = width - neededWidth;

        scene.changeLayer(layer.bgtile, {
            width: width,
            height: height
        });
        scene.changeLayer(layer.splashwhite, {
            width: width,
            height: height
        });
        

        var dims, f;

        if (width < height) {
            this.isPortrait = true;
            
            x /= 2;

            var bot = boardOffsetX;

            dims = {
                scoreLabel: { x: x + 90, y: boardOffsetX - 30 },
                rankLabel: { x: x + 145, y: boardOffsetX - 30 },
                timeLabel: { x: x + 177, y: boardOffsetX - 30 },
                rankMeterGray: { x: x + 150, y: boardOffsetX - 17 },
                profile: { x: x + 16, y: neededWidth + 14 },
                chatnotif: { x: x + 16, y: neededWidth + 14 },
                energy: { x: x + (neededWidth - 38), y: neededWidth + 10 },
                hud1: { x: 0, y: neededWidth },
                hud2: { x: 0, y: neededWidth },
                currentScore: { x: x + 110, y: boardOffsetX - 50 },
                currentRank: { x: x + 160, y: boardOffsetX - 50 },
                currentTime: { x: x + 200, y: boardOffsetX - 50 }
            };

            f = function(val) {
                return { x: val.x, y: val.y - neededWidth };
            };
        } else {
            dims = {
                scoreLabel: { x: 20, y: 95 },
                rankLabel: { x: 23, y: 140 },
                timeLabel: { x: 18, y: 185 },
                rankMeterGray: { x: 23, y: 153 },
                profile: { x: 14, y: 8 },
                chatnotif: { x: 14, y: 8 },
                energy: { x: 26, y: 170 },
                hud1: { x: 0, y: 0 },
                hud2: { x: 0, y: 0 },
                currentScore: { x: boardOffsetX / 2, y: 75 },
                currentRank: { x: boardOffsetX / 2, y: 120 },
                currentTime: { x: boardOffsetX / 2, y: 165 }
            };

            f = function(val) {
                return { x: val.x + neededWidth, y: val.y };
            };
        }

        this.dims     = dims;
        this.closed_f = f;

        // scene.changeLayer(layer.notifcation, f(dims.notification));

        scene.change(this.scoreLabel, dims.scoreLabel);
        scene.change(this.rankLabel, dims.rankLabel);
        scene.change(this.timeLabel, dims.timeLabel);
        scene.change(this.rankMeterGray, dims.rankMeterGray);

        // scene.changeLayer(layer.energy1, f(dims.energy));
        // scene.changeLayer(layer.energy2, f(dims.energy));

        scene.changeLayer(layer.hud1, f(dims.hud1));
        scene.changeLayer(layer.hud2, f(dims.hud2));

        this.currentScore.moveTo(dims.currentScore);
        this.currentRank.moveTo(dims.currentRank);
        this.currentTime.moveTo(dims.currentTime);

        scene.translate(layer.checkboard, x, 0);
        scene.translate(layer.gems, x, 0);
        scene.translate(layer.particles, x, 0);
        scene.translate(layer.pointer, x, 0);
        scene.translate(layer.movemarker, x, 0);
        scene.translate(layer.bgtile, 0, 0);
        scene.translate(layer.notification, x, -notifHeight);
        scene.translate(layer.notifcontent, x, -notifHeight);
        
        // position the pointer to center
        this.select(27);
        
        this.animateLayers(true);
        this.updateGems(app.chess.board.player);
    },
	
	checkBlock: function(index) {
		return true;
	},
	
	/**
	 * Used to draw the treasure on the screen, need a board as a parameter.
	 * will have 2 main condition, for own board & opponent board
	 */
	updateGems: function(selectedBoard) {
	    var view = this;
	    var scene = this.scene();
	    var xy;
	    var gem;
	    
	    console.log('Board for: ' + selectedBoard.side);
	    this.clearGems();
	    for (var n = 0; n < selectedBoard.map.length; n++) {
	        node = selectedBoard.map[n];
	        
	        if (node !== null) {
				spriteName = 'light';
				frame = node.spriteType;
				
				if (selectedBoard.side === app.chess.playerSide) {
					console.log('');
				} else {
					// this is opponent board drawing
					
					if (node.revealed) {
						console.log('REVEALED');
					} else if (node.peeked) {
						console.log('HALF REVEALED');
						spriteName = 'revealed';
						frame = 1;
					} else {
						spriteName = 'boardtile';
						frame = 1;
					}
				}
				
				xy = view.gemXY(n);
				obj = scene.add({
                    sprite: spriteName,
                    x: xy.x,
                    y: xy.y,
                    layer: layer.gems,
                    frame: frame
                });
                
                view.gems[node.id] = {
                    obj: obj,
                    x: xy.x,
                    y: xy.y,
                    index: n  
                };
	        }
	    }
	},
	
	digSoil: function(selectedBoard, index, isRemote) {
		var self = this;
		node = selectedBoard.map[index];
		if (node !== null) {
			selectedBoard.digTreasure(index);
		}
		
		if (!isRemote) {
			app.msg('digSoil', { index: index, opponentMap: selectedBoard.map });
			self.disableTurn();
			self.updateGems(app.chess.board.opponent);
			
			if (selectedBoard.isTreasureEmpty()) {
				// console.log('YOU WIN!!!!!');
				app.msg('handlePlayerWin', { });
			} else {
				setTimeout(function() {
					self.switchTurn();
				}, 2000);
			}
		} else {
	        self.updateGems(app.chess.board.player);
		}
	},
	
	clearGems: function() {
        var scene = this.scene();
        
        for (var k in this.gems) {
            var g = this.gems[k];
            scene.remove(g.obj);
        }
        
        this.gems = {};
    },
    
	clearTimeout: function() {
	    clearInterval(app.chess.interval);
	    this.currentTime.set('0:00');
	},
	
	setTimeout: function(remaining) {
	    var self = this;
	    
	    // reset timeoutfirst!
	    self.clearTimeout();
	    
	    app.chess.timeout = remaining;
	    app.chess.interval = setInterval(function() {
	        app.chess.timeout--;
	        
	        self.currentTime.set(app.chess.inMinute(app.chess.timeout));
	    }, 1000);
	    
	    self.currentTime.set(app.chess.inMinute(app.chess.timeout));
	},
	
	setTurn: function(side) {
	    var self = this;
	    
	    app.chess.turn = side;
	    if (self.isMultiplayer() && app.chess.isTurn()) {
	        self.activateProfile();
	        self.showPointer();
	        
	        var selectedBoard = app.chess.board.opponent;
	        console.log('Show player ' + selectedBoard.side + ' board');
	        self.updateGems(selectedBoard);
	        self.hideMarker();
	        
	        app.msg('moveCursor', { index: self.selected });
	    }
	},
	
	disableTurn: function() {
		var self = this;
		
		app.chess.turn = -1;
		self.hidePointer();
		self.clearTimeout();
		self.deactivateProfile();
	},
	
	switchTurn: function() {
	    var self = this;
	    
	    if (self.isMultiplayer()) {
	        
	        // Send switchTurn message to backend
            // params: { playerSide: app.chess.playerSide }
            app.msg('switchTurn', { side: app.chess.playerSide });
	        
	        // -1 means remote player
	        self.disableTurn();
	        
	        var selectedBoard = app.chess.board.player;
	        console.log('Show player ' + selectedBoard.side + ' board');
	        self.updateGems(selectedBoard);
	    }
	    
	    console.log('Turn Now: ' + app.chess.turn);
	},
	
	showChatScreen: function() {
	    var self = this;
	    
	    if (app.chess.players) {
	        app.pushView('chat', { });
	    } else {
	        self.showNotification('Please wait for another player');
	    }
	},
	
	setPlayers: function(light, dark) {
	   app.chess.players = { };
	   app.chess.players[0] = light;
	   app.chess.players[1] = dark;
	},
	
	stopGame: function() {
	    var self = this;
        
        // Delete the roomId	     
	    app.chess.storage.set('roomId', { });
        app.chess.turn = -1;
        app.chess.players = undefined;
        
        self.hidePointer();
        self.hideMarker();
        self.clearTimeout();
        
        /*
        var nodeObj;
        app.chess.board.map.forEach(function(item) {
            if (item !== null) {
                nodeObj = self.gems[item.id];
                self.explodeGem(nodeObj, true);
            }
        });
        */
	},
	
	gemXY: function(index) {
        return {
            x: Math.floor(index % 8) * 30,
            y: Math.floor(index / 8) * 30
        };
    },
	
	markMove: function(index) {
	    xy = this.gemXY(index);
	    this.scene().change(this.moveMarker, {
            x: xy.x - 6,
            y: xy.y - 6
        });
        this.scene().changeLayer(layer.movemarker, {
            visible: true
        });
	},
	
	hideMarker: function() {
	    this.scene().changeLayer(layer.movemarker, {
            visible: false
        });
	},
	
	hidePointer: function() {
        this.scene().changeLayer(layer.pointer, {
            visible: false
        });
    },
	
	showPointer: function() {
	    this.scene().changeLayer(layer.pointer, {
            visible: true
        });
        
        app.vibrate(150);
    },
	
	showChatNotif: function() {
	    this.scene().changeLayer(layer.chatnotif, {
            visible: true 
        });
        
        app.vibrate(100);  
	},
	
	hideChatNotif: function() {
	    this.scene().changeLayer(layer.chatnotif, {
            visible: false 
        });
	},
	
	showNotification: function(notifContent) {
	    var scene = this.scene();
	    
	    if (notifContent === undefined) {
	        
	        // Assign default notification value
	        notifContent = 'Press any key to hide this message, (1) to chat, (9) to quit this game';
	    } else {
	        app.vibrate(250);
	    }
	    
	    this.isNotificationShown = true;
	    scene.notificationContent.label(notifContent);
	    scene.animate(layer.notification, {
            y: 0
        });   
        scene.animate(layer.notifcontent, {
            y: 0
        }); 
        
        // Check whether it is end of game
        if (app.chess.state === gameStates.win || app.chess.state === gameStates.lose) {
            this.stopGame();
        }
	},
	
	hideNotification: function() {
	    var self = this;
        var scene = this.scene();
        
        self.isNotificationShown = false;
        scene.animate(layer.notifcontent, {
            y: -notifHeight,
            done: function() {
                scene.notificationContent.label('');
                if (app.chess.state === gameStates.win || app.chess.state === gameStates.lose) {
                    app.isBackFromGame = true;
                    self.clear();
                    self.clearTimeout();
                    
                    self.exitGame();
                }
            }
        }); 
        scene.animate(layer.notification, {
            y: -notifHeight
        });  
    },
	
	activateProfile: function() {
	    var scene = this.scene();
	    var self = this;
	    
	    scene.change(self.profileHolder, {
            frame: 0
        });
	},
	
	deactivateProfile: function() {
	    var scene = this.scene();
        var self = this;
        
        scene.change(self.profileHolder, {
            frame: 1
        });
	},
	
	select: function(index) {
	    this.selected = index;
	    
	    xy = this.gemXY(index);
	    this.scene().change(this.selObj, {
	        x: xy.x - 6,
	        y: xy.y - 6
	    });
	}
});
